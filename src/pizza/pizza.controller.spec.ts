import { Test, TestingModule } from '@nestjs/testing';
import { PizzaController } from './pizza.controller';
import { PizzaModule} from './pizza.module';
import { PizzaService } from './pizza.service';
import { Repository } from '../other/repository';

describe('Pizza Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [PizzaController],
      providers: [PizzaService, Repository],
      imports: [PizzaModule],
    }).compile();
  });
  it('should be defined', () => {
    const controller: PizzaController = module.get<PizzaController>(PizzaController);
    expect(controller).toBeDefined();
  });
});

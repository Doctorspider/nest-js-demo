import { Test, TestingModule } from '@nestjs/testing';
import { PizzaService } from './pizza.service';
import { PizzaModule } from './pizza.module';
import { Repository } from '../other/repository';

describe('PizzaService', () => {
  let service: PizzaService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PizzaService, Repository],
      imports: [PizzaModule],
    }).compile();
    service = module.get<PizzaService>(PizzaService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

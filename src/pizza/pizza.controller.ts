import { Controller, Get, Req, Param, Post, Body, Inject } from '@nestjs/common';
import { Pizza } from './Pizza';
import { IPizzaService } from './pizza.service';
import { PizzaInput } from './PizzaInput';

@Controller('pizza')
export class PizzaController {
    private _pizzaService: IPizzaService;

    constructor(@Inject('PizzaService') pizzaService: IPizzaService){
        this._pizzaService = pizzaService;
    }

    @Get()
    async getAll() {
        const items = await this._pizzaService.getPizzas();
        return {
            items,
        };
    }

    @Get(':name')
    getOne(@Param('name') name: string) {
        return `We don't have ${name} in stock`;
    }

    @Post()
    add(@Body() input: PizzaInput) {
        if (!Object.keys(input).length) {
            return `Who do ya tryin to fool, mate?`;
        }
        this._pizzaService.addPizza(new Pizza(input.Name));
        return `We added ${input.Name} to the list`;
    }
}

import { Injectable } from '@nestjs/common';
import { Pizza } from './Pizza';
import { Repository } from '../other/repository';

export interface IPizzaService{
    getPizzas(): Promise<Pizza[]>;
    addPizza(pizza: Pizza): void;
    getMessage(): string;
}

@Injectable()
export class PizzaService implements IPizzaService {
    private _pizzas: Pizza[] = [];

    constructor(private repository: Repository){

    }

    public getPizzas(): Promise<Pizza[]> {
        return (async (): Promise<Pizza[]> => this._pizzas)();
    }

    public addPizza(pizza: Pizza): void{
        this._pizzas.push(pizza);
    }

    public getMessage(): string{
        return this.repository.get();
    }
}

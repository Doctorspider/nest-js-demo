import { Module } from '@nestjs/common';
import { Repository } from '../other/repository';
import { PizzaService } from './pizza.service';
import { PizzaController } from './pizza.controller';

const pizzaProvider = {
    provide: 'PizzaService',
    useClass: PizzaService,
  };

@Module({
    controllers: [PizzaController],
    providers: [pizzaProvider, Repository],
})
export class PizzaModule {}
